<?php


class CSV
{
    private $path;

    public function __construct($path)
    {
        if (!file_exists($path)) {
            file_put_contents($path, '');
        }
        $this->path = $path;
    }

    public function saveToFile(array $array)
    {
        $fb = fopen($this->path, 'w');
        fputcsv($fb, array_keys($array[0]));
        foreach ($array as $value) {
            fputcsv($fb, $value);
        }
        fclose($fb);
    }
}