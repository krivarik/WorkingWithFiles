<?php
require_once __DIR__."/file.php";

echo "File: ".$dataFile->getFileName()."\n";
echo "Size: ".$dataFile->getFileSize()."\n";
echo $dataFile->getReadWrite()."\n";
echo "Created: ".$dataFile->getCreating()."\n";
echo "Modified: ".$dataFile->getLastModeration()."\n";
echo $dataFile->getContent();