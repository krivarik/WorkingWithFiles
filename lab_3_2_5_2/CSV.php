<?php


class CSV
{
    private $path;

    public function __construct($path)
    {
        if (!file_exists($path)) {
           echo "Файл не существует";
        }
        $this->path = $path;
    }

    public function saveToFile()
    {

        $fd=fopen($this->path, 'r');
        $arrayKey=fgetcsv($fd);
        while (($data=fgetcsv($fd))!== FALSE){
            $arrayData[]=array_combine($arrayKey,$data);
        }
        fclose($fd);
       
        print_r($arrayData);
    }
}