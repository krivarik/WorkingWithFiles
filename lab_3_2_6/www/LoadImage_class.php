﻿<?php
   /* класс загрузки графического файла на сервер
      - Провряет успешнось загрузки файла на сервер во временный каталог
      - Проверяет на соответствие одному из заданных форматов
      - Проверяет размер файла
      - Изменяет размер изоражения
      - Перемещает файт в указанную папку */

class LoadImage {
  private $flag=true; /** Флаг корректности выполненной проверки */
  private $namePole; /** Имя поля формы для загрузки картинки */
  private $messVerification; /** Сообщение при непройденой проверки или ошибке */
  private $validTrue=""; /** Путь к изображению или сообщение об удачной проверки. Может быть пустым */
  private $type; /** Тип загруженного файла */
  private $dataImg; /** массив из 7 элементов возвращаемых функцией getimagesize */
  private $size;/** Максимально допустимый размр загружаемого файла по умолчанию 500кб размер вводится в байтах */
  private $convertSize;/** размер в килобайтах для вывода на екран */
  private $massivConstant=array(png=>IMAGETYPE_PNG,jpeg=>IMAGETYPE_JPEG,gif=>IMAGETYPE_GIF); /* массив из констант типа изображения  */
  private $width; /** Новая высота изображения */
  private $height; /** Новая ширина изображения */
  private $image; /** Pесурс изображения в случае успеха, или FALSE в случае ошибки */

  /* устанавливает следующие свойства: $namePole, $size, $validTrue, $convetSize */

  public function __construct ($namePole, $validTtrue="", $size=512000){
    $this->namePole=$namePole;
    $this->validTrue=$validTtrue;
    $this->size=$size;
    $this->convertSize=$size/1024;
  }

  /* метод производит проверку загрузки файла и его размер, а также на небольшую
   * проверку от изменения скрытого поля в форме */

   public function ControleLoad(){
     /* Проверяем загружен файл или нет */
     if (!is_uploaded_file($_FILES[$this->namePole]["tmp_name"])) {
       /* записываем в свойство flag значение false */
       $this->flag=false;

       /* сверяем коды ошибок в массиве $_FILES и выдаем соответствующее сообщение */
       switch ($_FILES[$this->namePole]["error"]) {
         case 1: case 2:
         $this->messVerification="Размер изображения больше{$this->convertSize} Kb";
         break;
         case 3: case 4:
         $this->messVerification="Не получилось загрузить изображение";
         break;

       }
      return(false);
     } else {
       $this->size>$_FILES[$this->namePole]["size"] or die("произошли изменения с формой");
     }
     return(true);
   }

  /* метод возвращает свойство $flag */
    public function GetFlag(){
      return ($this->flag);
    }


    /* Метод определяет что файл является изображением и имеет нужный формат */
    public function IsImage (){
      /** Устанавливаем свойство $dataImg. Если значения в функцию не передаются (пустая строка)
        * выводится сообщение об ошибке нужно подавить его символом @, если передать не изображение
        * а другой файл, будет пустой результат
        **/
      $this->dataImg=@getimagesize($_FILES[$this->namePole]["tmp_name"]);
      /* Определяет расширение файла из типа файла */
      $this->type=explode('/', $_FILES[$this->namePole]["type"]);
      if (!($this->dataImg[0]>0 and $this->dataImg[1]>0 and $this->dataImg[2]==$this->massivConstant[$this->type[1]])) {
        $this->flag=false;
        $this->messVerification="Файл должен быть изображением и иметь формат jpeg, jpg, png, gif";
      }

    }
    /* функция для изменения размера изображения */
    public function ChangeSizeImage ($width, $height, $newfileName){
      switch ($this->dataImg[2]) {
        case IMAGETYPE_JPEG:
        /* пытаемся создать ресурс изображения */
          $this->image=@imagecreatefromjpeg($_FILES[$this->namePole]["tmp_name"]);
          /* Усли не удалось  */
          if (!$this->image) {
            /* Пишем сообщение об ошибке и возвращаем ошибку */
             $this->messVerification="Не получилось создать изображение";
             return(false);
          }
          $this->NewImage($width, $height);
          /* сохраняем новое изображение */
          if (!@imagejpeg($this->image, "{$newfileName}.{$this->type[1]}", 75) ) {
            /* Пишем сообщение об ошибке и возвращаем ошибку */
            $this->messVerification="Не удалось сохранить изображение";
            return(false);
          }
        break;
        case IMAGETYPE_GIF:
          $this->image=@imagecreatefromgif($_FILES[$this->namePole]["tmp_name"]);
          if (!$this->image) {
             $this->messVerification="Не получилось создать изображение";
             return(false);
          }
          $this->NewImage($width, $height);
          if (!@imagegif($this->image, "{$newfileName}.{$this->type[1]}", 75) ) {
            $this->messVerification="Не удалось сохранить изображение";
            return(false);
          }
        break;
        case IMAGETYPE_PNG:
          $this->image=@imagecreatefrompng($_FILES[$this->namePole]["tmp_name"]);
          if (!$this->image) {
             $this->messVerification="Не получилось создать изображение";
             return(false);
          }
          $this->NewImage($width, $height);
          if (!@imagepng($this->image, "{$newfileName}.{$this->type[1]}", 75) ) {
            $this->messVerification="Не удалось сохранить изображение";
            return(false);
          }
        break;
        default:
          die ("Не получилось создать изображение.");
      }
      chmod("{$newfileName}.{$this->type[1]}", 0444);
      @unlink($_FILES[$this->namePole]["tmp_name"]);

    }
    /* функция для создания нового изображения */
    private function NewImage ($width, $height){
      $new_image=imagecreatetruecolor($width, $height);
      imagecopyresized($new_image, $this->image, 0, 0, 0, 0, $width, $height, imagesx($this->image), imagesy($this->image));
      $this->image=$new_image;
    }

    /* функция вывода изображения в браузер */
    function output ($width, $height, $filename){
      $this->dataImg=@getimagesize($filename);
      switch ($this->dataImg[2]) {
        case IMAGETYPE_JPEG:
          $this->image=@imagecreatefromjpeg($filename);
          if (!$this->image) {
             $this->messVerification="Не получилось создать изображение";
             return(false);
          }
          $this->NewImage($width, $height);
          imagejpeg($this->image);
        break;
        case IMAGETYPE_GIF:
          $this->image=@imagecreatefromgif($filename);
          if (!$this->image) {
             $this->messVerification="Не получилось создать изображение";
             return(false);
          }
          $this->NewImage($width, $height);
          imagegif($this->image);
        break;
        case IMAGETYPE_PNG:
          $this->image=@imagecreatefrompng($filename);
          if (!$this->image) {
             $this->messVerification="Не получилось создать изображение";
             return(false);
          }
          $this->NewImage($width, $height);
          imagepng($this->image);
        break;
        default:
          die ("Не получилось создать изображение.");
       }
    }
    /* Выводит сообщение */
    public function GetMessVer(){
      return ($this->messVerification);
    }

    /* тестовые функции их можно и удалить */
    public function GettestTrue(){
      return ($this->validTrue);
    }
    public function GettestName(){
      return ($this->namePole);
    }
    public function datImg(){
      return ($this->dataImg);
    }
    public function tip(){
      return ($this->type);
    }
}

?>