<?php


class file
{
    private $pathFile;

    public function __construct($pathFile)
    {
        if (!file_exists($pathFile)) {
            throw new Exception('Файл не существует');
        }
        $this->pathFile = $pathFile;
    }

    public function getFileName()
    {
        return basename($this->pathFile);
    }

    public function getFileSize()
    {
        if (filesize($this->pathFile)<1024){
            return filesize($this->pathFile)." bytes";
        }elseif (filesize($this->pathFile)<1048576){
            $kB=filesize($this->pathFile)/1024;
            return $kB."  kB";
        }elseif (filesize($this->pathFile)<1073741824){
            $mB=(filesize($this->pathFile)/1024)/1024;
            return $mB."  MB";
        }
    }

    public function getReadWrite()
    {
        if (is_readable($this->pathFile)) {
            return 'is readable';
        } elseif(is_writable($this->pathFile)) {
            return 'is writable';
        }
    }

    public function getCreating()
    {
        return date("F d Y H:i:s.", filectime($this->pathFile));
    }

    public function getLastModeration()
    {
        return date("F d Y H:i:s.", filemtime($this->pathFile));
    }
}

echo `chcp 65001`;

try{
    $dataFile=new file(__DIR__."/WARNING.doc");
}
catch(Exception $e){
    die($e->getMessage());
}