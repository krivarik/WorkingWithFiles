<?php


/**
 * Class file
 */
class file
{
    /**
     * @var
     */
    private $pathFile;

    /**
     * file constructor.
     * @param $pathFile
     */
    public function __construct($pathFile)
    {
        if (!file_exists($pathFile)) {
            file_put_contents($pathFile, '');
        }
        $this->pathFile = $pathFile;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return basename($this->pathFile);
    }

    /**
     * @return string
     */
    public function getFileSize()
    {
        if (filesize($this->pathFile) < 1024) {
            return filesize($this->pathFile) . " bytes";
        } elseif (filesize($this->pathFile) < 1048576) {
            $kB = filesize($this->pathFile) / 1024;
            return $kB . "  kB";
        } elseif (filesize($this->pathFile) < 1073741824) {
            $mB = (filesize($this->pathFile) / 1024) / 1024;
            return $mB . "  MB";
        }
    }

    /**
     * @return string
     */
    public function getReadWrite()
    {
        if (is_readable($this->pathFile)) {
            return 'is readable';
        } elseif (is_writable($this->pathFile)) {
            return 'is writable';
        }
    }

    /**
     * @return bool|string
     */
    public function getCreating()
    {
        return date("F d Y H:i:s.", filectime($this->pathFile));
    }

    /**
     * @return bool|string
     */
    public function getLastModeration()
    {
        return date("F d Y H:i:s.", filemtime($this->pathFile));
    }

    /**
     * @return string
     */
    public function getContent()
    {
        $handler = fopen($this->pathFile, 'r');
        $content = "";
        while (!feof($handler)) {
            $content .= fgets($handler);
        }
        fclose($handler);
        return $content;
    }

    /**
     * @param $numberRows
     * @param $length
     */
    public function write($numberRows, $length)
    {
        $fp=fopen($this->pathFile, "w");
        for ($i=0; $i<$numberRows; $i++){
            fwrite($fp,Strings::getRandomString($length) );
        }
        fclose($fp);
    }
}





$dataFile=new file(__DIR__."/data.txt");


